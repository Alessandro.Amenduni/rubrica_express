var express = require('express');
const ejs = require('ejs');
var app = express();
var fs = require('fs');
const Sequelize = require('sequelize');
var path = require('path');
app.use(express.static(path.join(__dirname, 'public')));


app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(express.json());



// db connection
let sequelize = new Sequelize('rubrica', 'rubrica', 'rubrica@2020', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

// sync models
let Contacts = sequelize.define('contacts', {
    name: {
        type: Sequelize.STRING,
        field: 'name',
    }
},
    { freezeTableName: true }
);

let Telephones = sequelize.define('telephones', {
    telephone: {
        type: Sequelize.STRING,
        field: 'telephone',
    },
    type: {
        type: Sequelize.ENUM('cellulare', 'fisso', 'fax'),
        field: 'type'
    }
},
    { freezeTableName: true }

);
///association//

Contacts.hasMany(Telephones, { onDelete: 'CASCADE' });
Telephones.belongsTo(Contacts);
//end connection 

sequelize.sync({ force: false }).then(() => {

//inserisco i nomi all'interno dell'array nomi//
var nomi = [];

var ricerca={};

function aggiungo() {
    Contacts.findAll({
        attributes: ['id', 'name']
    }).then((results) => {
        results.forEach((result) => {nomi.push({ id: `${result.id}`, name: `${result.name}` }) });
    });
}
aggiungo();

////inizio a creare il progetto REST/////
app.get('/', (req, res) => {
    res.render('index');
    
});

app.get('/aggiungi', function (req, res) {
    Contacts.findAll({
        attributes: ['id', 'name'],
        include: [{
            model: Telephones,
            attributes: ['telephone', 'type']
        }]
    }).then(contacts => {
        res.json(contacts);
    })
});

app.get('/cerca', function (req, res) {
    ricerca.telephone=[];
    var CercaNome=req.query.nome;
    sequelize.query("SELECT name, telephone, type FROM contacts JOIN telephones ON contacts.id=telephones.contactId WHERE name = '" + CercaNome + "'").then((results) => {
        ricerca.name=CercaNome;
        results[0].forEach((ris) => {ricerca.telephone.push({ telephones: `${ris.telephone}`,type:`${ris.type}`})});
        res.json(ricerca);
    })
    console.log(CercaNome);
})

var esiste;
//controllo se il nome è gia stato inserito//
function controllo_nome(nome) {
    esiste = false;
    for (let j = 0; j < nomi.length && esiste == false; j++) {
        if (nome == nomi[j].name) {
            esiste = true;
        }
    }
}
///aggiungo un contatto non esistente////
app.post('/aggiungi', function (req, res) {
    var Name = req.body.name;
    var Number = req.body.telephone;
    var Type = req.body.type;
    controllo_nome(Name);
    console.log(esiste);
    if (esiste == false) {
            Contacts.create({
                name: Name,
                telephones: [{
                    telephone: Number,
                    type: Type
                }]
            }, {
                include: [Telephones]
            }).then(contacts => {
                nomi.push({ id: contacts.id, name: Name });
                nomi.forEach((ris) => console.log("then aggiungo Contacts  nome" + ris.name + "  id" + ris.id));
                res.json({ msg: `contacts: ${contacts} created` });
            })
    } else if(esiste==true){
        Telephones.create({ telephone: Number, type: Type }).then(telephones => {
            Contacts.findOne({ where: { name: Name } }).then(contacts => {
                contacts.addTelephones(telephones);
            })
        }).then(telephones => {
            res.json({ msg: `contacts: ${telephones} created` });
        });
    }
});

app.put('/aggiorna/:id', function (req, res) {
    var id1 = req.params.id;
    var newName = req.body.name;
    var found=false;

    for (let j = 0; j < nomi.length && found == false; j++) {
        if (nomi[j].id == Number(id1)) {
            console.log("aggiorno");
            esiste = true;
            nomi[j]={id:id1,name:newName};
        }
    }

    Contacts.update(
        { name: newName },
        { where: { id: id1  } 
    }).then(
        res.send("update")
    );
    
});

app.put('/aggiornanumero/:telephone', function (req, res) {
    var num = req.params.telephone;
    var tel=req.body.telephone;
    var tipo=req.body.type;
    Telephones.update(
        { telephone: tel,type:tipo },
        { where: { telephone: num  } 
    }).then(
        res.send("update numero")
    );
    
});
//elimina contatto///
app.delete('/elimina/:id', function (req, res) {
    var found = false;
    nomi.forEach((ris) => console.log("nome" + ris.name + "  id" + ris.id + '\n'));
    var id1 = req.params.id;
    for (let j = 0; j < nomi.length && found == false; j++) {
        if (nomi[j].id == Number(id1)) {
            console.log("elimino");
            esiste = true;
            nomi.splice(j, 1);
        }
    }
    Contacts.destroy({
        where: {
            id: id1
        }
    }).then(
        res.send("delete contact")
    );
});

//elimina numero//
app.delete('/eliminare/:telephone', function (req, res) {
    var telephone = req.params.telephone;
    Telephones.destroy({
        where: {
            telephone: telephone
        }
    }).then(
        res.send('Successfully deleted number!')
    )
});
app.listen(3000, function () {
    console.log('Server listening on ' + 3000);
});
});